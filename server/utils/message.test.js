var expect = require('expect');

var {generateMessage, generateLocationMessage} = require("./message");

describe('generateMessage', () => {
    it('should generate the correct message object', () => {
        var from = "Admin";
        var text = "Hello There";
        var res = generateMessage(from, text);

        expect(typeof res.createdAt).toBe('number');
        expect(res).toMatchObject({from, text}); 
    });
});

describe('generateLocationMessage', () => {
    it('should generate correct location object', () => {
        var from = "Admin";
        var latitude = 1;
        var longitude = 1;
        var url = "https://www.google.com/maps?q=1,1"
        var res = generateLocationMessage(from, latitude, longitude);

        expect(typeof res.createdAt).toBe('number');
        expect(res).toMatchObject({from, url}); 
    });
});
